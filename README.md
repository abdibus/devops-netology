1.
```
adib@devops:~/Documents/terraform$ git log | grep aefea
commit aefead2207ef7e2aa5dc81a34aedf0cad4c32545
commit 8619f566bbd60bbae22baefea9a702e7778f8254
commit 3593ea8b0aefea1b4b5e14010b4453917800723f
commit 0196a0c2aefea6b85f495b0bbe32a855021f0a24

adib@devops:~/Documents/terraform$ git log aefead2207ef7e2aa5dc81a34aedf0cad4c32545
commit aefead2207ef7e2aa5dc81a34aedf0cad4c32545
Author: Alisdair McDiarmid <alisdair@users.noreply.github.com>
Date:   Thu Jun 18 10:29:58 2020 -0400
   Update CHANGELOG.md
```
Answer: aefead2207ef7e2aa5dc81a34aedf0cad4c32545, Update CHANGELOG.md


2.
```
adib@devops:~/Documents/terraform$ git describe --exact-match 85024d3
v0.12.23
```
Answer: v0.12.23
<br>
used: <a href="https://stackoverflow.com/questions/1474115/how-to-find-the-tag-associated-with-a-given-git-commit">StackOverFlow</a>


3.
```
adib@devops:~/Documents/terraform$ git log --oneline --graph b8d720
*   b8d720f83 Merge pull request #23916 from hashicorp/cgriggs01-stable
|\
| * 9ea88f22f add/update community provider listings
|/
*   56cd7859e Merge pull request #23857 from hashicorp/cgriggs01-stable
|\
| * ffbcf5581 [Website]add checkpoint links
|/
* 58dcac4b7 (tag: v0.12.19) v0.12.19
```
Answer: "b8d720f83" has two parents, "9ea88f22f" and "56cd7859e"


4.
```
adib@devops:~/Documents/terraform$ git log v0.12.23..v0.12.24
commit 33ff1c03bb960b332be3af2e333462dde88b279e (tag: v0.12.24)
Author: tf-release-bot <terraform@hashicorp.com>
Date:   Thu Mar 19 15:04:05 2020 +0000

    v0.12.24

commit b14b74c4939dcab573326f4e3ee2a62e23e12f89
Author: Chris Griggs <cgriggs@hashicorp.com>
Date:   Tue Mar 10 08:59:20 2020 -0700

    [Website] vmc provider links

commit 3f235065b9347a758efadc92295b540ee0a5e26e
Author: Alisdair McDiarmid <alisdair@users.noreply.github.com>
Date:   Thu Mar 19 10:39:31 2020 -0400

    Update CHANGELOG.md

commit 6ae64e247b332925b872447e9ce869657281c2bf
Author: Alisdair McDiarmid <alisdair@users.noreply.github.com>
Date:   Thu Mar 19 10:20:10 2020 -0400

    registry: Fix panic when server is unreachable

    Non-HTTP errors previously resulted in a panic due to dereferencing the
    resp pointer while it was nil, as part of rendering the error message.
    This commit changes the error message formatting to cope with a nil
    response, and extends test coverage.

    Fixes #24384

commit 5c619ca1baf2e21a155fcdb4c264cc9e24a2a353
Author: Nick Fagerlund <nick.fagerlund@gmail.com>
Date:   Wed Mar 18 12:30:20 2020 -0700

    website: Remove links to the getting started guide's old location

    Since these links were in the soon-to-be-deprecated 0.11 language section, I
    think we can just remove them without needing to find an equivalent link.

commit 06275647e2b53d97d4f0a19a0fec11f6d69820b5
Author: Alisdair McDiarmid <alisdair@users.noreply.github.com>
Date:   Wed Mar 18 10:57:06 2020 -0400

    Update CHANGELOG.md

commit d5f9411f5108260320064349b757f55c09bc4b80
Author: Alisdair McDiarmid <alisdair@users.noreply.github.com>
Date:   Tue Mar 17 13:21:35 2020 -0400

    command: Fix bug when using terraform login on Windows

commit 4b6d06cc5dcb78af637bbb19c198faff37a066ed
Author: Pam Selle <pam@hashicorp.com>
Date:   Tue Mar 10 12:04:50 2020 -0400

    Update CHANGELOG.md

commit dd01a35078f040ca984cdd349f18d0b67e486c35
Author: Kristin Laemmert <mildwonkey@users.noreply.github.com>
Date:   Thu Mar 5 16:32:43 2020 -0500

    Update CHANGELOG.md

commit 225466bc3e5f35baa5d07197bbc079345b77525e
Author: tf-release-bot <terraform@hashicorp.com>
Date:   Thu Mar 5 21:12:06 2020 +0000

    Cleanup after v0.12.23 release
```
used: Borue lessons && <a href="https://stackoverflow.com/questions/8136178/git-log-between-tags">StackOverFlow</a>


5.
To find function definition:
```
adib@devops:~/Documents/terraform$ git grep "func providerSource("
provider_source.go:func providerSource(configs []*cliconfig.ProviderInstallation, services *disco.Disco) (getproviders.Source, tfdiags.Diagnostics) {
```

To find commit:
```
adib@devops:~/Documents/terraform$ git log -S "func providerSource(configs []*cliconfig.ProviderInstallation, services *disco.Disco) (getproviders.Source, tfdiags.Diagnostics)" --oneline
5af1e6234 main: Honor explicit provider_installation CLI config when present
```

To watch commit:
```
adib@devops:~/Documents/terraform$ git log | grep 5af1e6234
commit 5af1e6234ab6da412fb8637393c5a17a1b293663
adib@devops:~/Documents/terraform$ git log 5af1e6234ab6da412fb8637393c5a17a1b293663
commit 5af1e6234ab6da412fb8637393c5a17a1b293663
Author: Martin Atkins <mart@degeneration.co.uk>
Date:   Tue Apr 21 16:28:59 2020 -0700

    main: Honor explicit provider_installation CLI config when present

    If the CLI configuration contains a provider_installation block then we'll
    use the source configuration it describes instead of the implied one we'd
    build otherwise.
```
Answer: 5af1e6234ab6da412fb8637393c5a17a1b293663


6.
```
adib@devops:~/Documents/terraform$ git log -S "globalPluginDirs" --oneline
35a058fb3 main: configure credentials from the CLI config file
c0b176109 prevent log output during init
8364383c3 Push plugin discovery down into command package
```
Answer: "35a058fb3", "c0b176109", "8364383c3"


7.
```
adib@devops:~/Documents/terraform$ git log -S "synchronizedWriters"
commit bdfea50cc85161dea41be0fe3381fd98731ff786
Author: James Bardin <j.bardin@gmail.com>
Date:   Mon Nov 30 18:02:04 2020 -0500

    remove unused

commit fd4f7eb0b935e5a838810564fd549afe710ae19a
Author: James Bardin <j.bardin@gmail.com>
Date:   Wed Oct 21 13:06:23 2020 -0400

    remove prefixed io

    The main process is now handling what output to print, so it doesn't do
    any good to try and run it through prefixedio, which is only adding
    extra coordination to echo the same data.

commit 5ac311e2a91e381e2f52234668b49ba670aa0fe5
Author: Martin Atkins <mart@degeneration.co.uk>
Date:   Wed May 3 16:25:41 2017 -0700

    main: synchronize writes to VT100-faker on Windows

    We use a third-party library "colorable" to translate VT100 color
    sequences into Windows console attribute-setting calls when Terraform is
    running on Windows.

    colorable is not concurrency-safe for multiple writes to the same console,
    because it writes to the console one character at a time and so two
    concurrent writers get their characters interleaved, creating unreadable
    garble.

    Here we wrap around it a synchronization mechanism to ensure that there
    can be only one Write call outstanding across both stderr and stdout,
    mimicking the usual behavior we expect (when stderr/stdout are a normal
    file handle) of each Write being completed atomically.
(END)
```
Answer: Martin Atkins, because he has made 1st commit

